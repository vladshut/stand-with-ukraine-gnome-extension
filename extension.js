/* extension.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

/* exported init */

const GETTEXT_DOMAIN = 'stand-with-ukraine';

const { GObject, St, Gtk } = imports.gi;


const ExtensionUtils = imports.misc.extensionUtils;
const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;
const Clutter   = imports.gi.Clutter


const _ = ExtensionUtils.gettext;

var Flag = GObject.registerClass(
    class FlagIndicator extends PanelMenu.Button {
        _init(text) {
            super._init(0.0, _('Stand With Ukraine'))

            this._label = new St.Label({ y_align: Clutter.ActorAlign.CENTER })
            this.add_actor(this._label);
            this.add_child(this._label);

            this.setText('🇺🇦');
            this.setVisible(true);

            this.addLink(_('United 24'), 'https://u24.gov.ua/');
            this.addLink(_('All news'), 'https://war.ukraine.ua/news/');
            this.addLink(_('Support'), 'https://war.ukraine.ua/support-ukraine/');
            this.addLink(_('Russia\'s war crimes'), 'https://war.ukraine.ua/russia-war-crimes/');
            this.addLink(_('How to donate?'), 'https://war.ukraine.ua/donate/');
            this.addLink(_('Russian loses'), 'https://www.minusrus.com/en');
            this.addLink(_('MAKE A DONATION'), 'https://bank.gov.ua/en/news/all/natsionalniy-bank-vidkriv-spetsrahunok-dlya-zboru-koshtiv-na-potrebi-armiyi');
        }

        addLink(label, url) {
            let item = new PopupMenu.PopupMenuItem(_(label));
            item.connect('activate', () => {
                this.menu.close();
                try {
                    Gtk.show_uri(null, url, global.get_current_time());
                }
                catch (err) {
                    let title = _("Can not open %s").format(url);
                    Main.notifyError(title, err);
                }
            });

            this.menu.addMenuItem(item);
        }

        setText(text) {
            this._label.set_text(text)
        }

        setVisible(visible) {
            this.container.visible = visible
        }
    }
)

class Extension {
    constructor(uuid) {
        this._uuid = uuid;
    }

    enable() {
        this._label = new Flag();
        Main.panel.addToStatusArea(this._uuid, this._label);
    }

    disable() {
        if (this._label) {
            this._label.destroy();
            this._label = null;
        }
    }
}

function init(meta) {
    ExtensionUtils.initTranslations(GETTEXT_DOMAIN);

    return new Extension(meta.uuid);
}
